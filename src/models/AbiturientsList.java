package src.models;

import java.util.Arrays;

public class AbiturientsList {

    private Abiturient[] abiturients = new Abiturient[10];

    public Abiturient[] getAbiturients() {
        return abiturients;
    }

    @Override
    public String toString() {
        return "AbiturientsList{" +
                "abiturients=" + Arrays.toString(abiturients) +
                '}';
    }
}
