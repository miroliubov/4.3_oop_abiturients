package src.models;

import src.service.EnrollmentService;
import src.service.IEnrollmentService;

import java.util.Arrays;

public class Abiturient implements Comparable<Abiturient> {

    private String abiturientName;
    private int[] examMarks;
    private double averageMark;
    private IEnrollmentService enrollmentService;

    public Abiturient(String abiturientName, int[] examMarks) {
        this.abiturientName = abiturientName;
        this.examMarks = examMarks;
        enrollmentService = new EnrollmentService();
        calculateAverageMark(examMarks);
    }

    private void calculateAverageMark(int[] examMarks) {
        this.averageMark = enrollmentService.averageMark(examMarks);
    }

    @Override
    public int compareTo(Abiturient o) {
        return Double.compare(o.averageMark, this.averageMark);
    }

    @Override
    public String toString() {
        return  "\n" + "Абитуриент " + abiturientName + '\'' +
                ", Оценки за экзамены = " + Arrays.toString(examMarks) +
                ", Средняя оценка = " + averageMark;
    }
}
