package src.demo;

import src.models.Abiturient;
import src.models.AbiturientsList;
import src.service.EnrollmentService;
import src.service.IEnrollmentService;

import java.util.Arrays;

public class DemoService implements IDemoService {

    @Override
    public void execute() {

        AbiturientsList abiturientsList = new AbiturientsList();
        IEnrollmentService enrollmentService = new EnrollmentService();

        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Ivanov I I"), new int[]{5, 5, 5, 5, 5}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Petrov P P"), new int[]{4, 4, 5, 4, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Smirnov S S"), new int[]{4, 5, 4, 5, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Vasiljev V V"), new int[]{5, 3, 3, 5, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Goncharov G T"), new int[]{4, 4, 3, 4, 3}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Olegov O O"), new int[]{4, 5, 4, 3, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Karavaev I S"), new int[]{5, 4, 4, 4, 5}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Nikolenko K K"), new int[]{4, 5, 3, 4, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Araraev H P"), new int[]{3, 3, 3, 5, 4}), abiturientsList);
        enrollmentService.addAbiturientToAbiturientsList(new Abiturient(("Lomaev A L"), new int[]{3, 5, 4, 4, 3}), abiturientsList);

        System.out.println("Список поступивших");
        System.out.println(Arrays.toString(enrollmentService.listOfEnrollment(abiturientsList, 5)));
    }
}
