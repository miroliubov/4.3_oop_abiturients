package src.service;

import src.models.Abiturient;
import src.models.AbiturientsList;

import java.util.Arrays;

public class EnrollmentService implements IEnrollmentService {

    @Override
    public void addAbiturientToAbiturientsList(Abiturient abiturient, AbiturientsList abiturientsList) {
        for (int i = 0; i < abiturientsList.getAbiturients().length; i++) {
            if (abiturientsList.getAbiturients()[i] == null) {
                abiturientsList.getAbiturients()[i] = abiturient;
                break;
            }
        }
    }

    @Override
    public double averageMark(int[] examMarks) {
        double sum = 0;
        int counter = 0;
        for (int examMark : examMarks) {
            sum = sum + examMark;
            counter++;
        }
        return Math.round((sum / (counter * 1.0)) * 100.0)/ 100.0;
    }

    @Override
    public Abiturient[] listOfEnrollment(AbiturientsList abiturientsList, int numberOfAbiturients) {
        if (abiturientsList.getAbiturients().length < numberOfAbiturients) {
            return abiturientsList.getAbiturients();
        }
        else {
            Arrays.sort(abiturientsList.getAbiturients());
            return Arrays.copyOf(abiturientsList.getAbiturients(), numberOfAbiturients);
        }
    }




}
