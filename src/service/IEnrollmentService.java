package src.service;

import src.models.Abiturient;
import src.models.AbiturientsList;

public interface IEnrollmentService {

    void addAbiturientToAbiturientsList(Abiturient abiturient, AbiturientsList abiturientsList);

    double averageMark(int[] marks);

    Abiturient[] listOfEnrollment(AbiturientsList abiturientsList, int numberOfAbiturients);

}
